﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticFilmPlanification.Models
{
    class FilmingCalendar
    {
        List<Scene> Scenes { get; set; }
        int Cost { get; set; }
    }
}
