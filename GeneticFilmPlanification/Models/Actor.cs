﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticFilmPlanification.Models
{
    class Actor
    {
        int CostPerDay { get; set; }
        int FirstParticipation { get; set; }
        int LastParticipation { get; set; }
        String ID { get; set; }
    }
}
