﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticFilmPlanification.Models
{
    class Scene
    {
        // se calcula en base a las páginas que la escena ocupa en el guión
        int Duration { get; set; }
        int Pages { get; set; }
        List<String> ActorsID { get; set; }
        Location Location { get; set; }
        bool Schedule { get; set; } // true = dia, false = noche
    }
}
