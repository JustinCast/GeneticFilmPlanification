﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticFilmPlanification.Models
{
    class Day
    {
        Time DayTime { get; set; }
        Time NightTime { get; set; }
        int DayNumber { get; set; }
    }
}
